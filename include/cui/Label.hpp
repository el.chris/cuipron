#pragma once

#include "cui/BaseWidget.hpp"
#include "cui/widgetProperties_t.hpp"
#include "cuipron/ConsoleFont.hpp"
#include "cuipron/Renderer.hpp"

#include <chrono>

namespace cui
{
class Label : public BaseWidget
{
  public:
    Label( cp::Renderer* const       renderer,
           const widgetProperties_t& properties ) noexcept;

    void
    Draw() noexcept override;
    void
    SetText( const set_t< std::string >& text ) noexcept;

  protected:
    set_t< std::string >                  textCallback;
    std::string                           currentText;
    uint64_t                              currentTextSize;
    uint64_t                              currentPosition{ 0 };
    uint64_t                              cleanupPosition{ 0 };
    uint64_t                              underlineRetreatPosition{ 0 };
    uint64_t                              updateSpeedInMs{ 10 };
    uint64_t                              retreatTimeMultiplier{ 4 };
    std::chrono::steady_clock::time_point lastUpdate;

    modList_t labelNormal{ cp::Background_Default, cp::Foreground_Default };
    modList_t labelOld{ cp::Background_Default, cp::Foreground_DarkGray };
    modList_t labelBuildup{ cp::Background_Default, cp::Foreground_Default,
                            cp::Style_Underline };
    modList_t labelBuildupGlow{ cp::Background_Default, cp::Foreground_Blue,
                                cp::Style_Underline, cp::Style_Bold };
};
} // namespace cui
