#pragma once

#include "cuipron/vec2.hpp"

namespace cui
{
struct widgetProperties_t
{
    cp::vec2 size{0, 0};
    cp::vec2 position{0, 0};
};
} // namespace cui
