#pragma once

#include "cui/BaseWidget.hpp"
#include "cui/widgetProperties_t.hpp"
#include "cuipron/ConsoleFont.hpp"
#include "cuipron/Renderer.hpp"

namespace cui
{
class ProgressBar : public BaseWidget
{
  public:
    ProgressBar( cp::Renderer* const       renderer,
                 const widgetProperties_t& properties ) noexcept;
    void
    Draw() noexcept override;
    void
    SetProgress( const set_t< int >& progress ) noexcept;
    void
    SetDescription( const set_t< std::string >& description ) noexcept;

  protected:
    set_t< uint16_t >    progressCallback;
    set_t< std::string > descriptionCallback;

    modList_t progressBarActive{ cp::Background_Green, cp::Foreground_Black };
    modList_t progressBarFinished{ cp::Background_LightBlue,
                                   cp::Foreground_Black };
    modList_t progressBarInactive{ cp::Style_Underline, cp::Foreground_Green,
                                   cp::Foreground_LightYellow, cp::Style_Bold };
};
} // namespace cui
