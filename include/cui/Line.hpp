#pragma once

#include "cui/BaseWidget.hpp"
#include "cui/widgetProperties_t.hpp"

#include <limits>

namespace cui
{
class Line : public BaseWidget
{
  public:
    enum Type
    {
        Vertical,
        Horizontal,
    };

    enum Style
    {
        Thin,
        Fat,
    };

    static constexpr uint64_t INFINITE = std::numeric_limits< uint64_t >::max();

    Line( cp::Renderer* const       renderer,
          const widgetProperties_t& properties ) noexcept;
    void
    SetUp( const Type type, const Style style, const uint64_t length ) noexcept;

    void
    Draw() noexcept override;

  private:
    Type     lineType = Type::Vertical;
    uint64_t lineLength{ 0u };
    Style    lineStyle;

    modList_t fatLineStyle{ cp::Background_LightGray, cp::Foreground_Default };
    modList_t thinLineStyle{ cp::Background_Default, cp::Foreground_LightGray };
};
} // namespace cui
