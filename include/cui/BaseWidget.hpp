#pragma once

#include "cui/widgetProperties_t.hpp"
#include "cuipron/ConsoleFont.hpp"
#include "cuipron/Renderer.hpp"
#include "cuipron/ThreadsafeQueue.hpp"
#include "cuipron/vec2.hpp"

#include <vector>

namespace cui
{
class BaseWidget
{
  public:
    template < typename T >
    using set_t = std::function< T() >;

    BaseWidget( cp::Renderer* const       renderer,
                const widgetProperties_t& properties ) noexcept;
    virtual ~BaseWidget() = default;

    cp::vec2
    GetSize() const noexcept;
    void
    SetSize( const cp::vec2& value ) noexcept;
    cp::vec2
    GetPosition() const noexcept;
    void
    SetPosition( const cp::vec2& value ) noexcept;

    friend class Cui;

  protected:
    virtual void
    Draw() noexcept;

  protected:
    using modList_t = std::vector< cp::Modifier >;
    cp::Renderer* const renderer{nullptr};
    cp::vec2            size;
    cp::vec2            position;

    mutable cp::ThreadsafeQueue< std::function< void() > > setter;
};
} // namespace cui
