#pragma once

#include "cui/BaseWidget.hpp"
#include "cui/widgetProperties_t.hpp"
#include "cuipron/ConsoleFont.hpp"
#include "cuipron/Renderer.hpp"
#include "cuipron/SystemCommand.hpp"

#include <string>

namespace cui
{
class CommandState : public BaseWidget
{
  public:
    CommandState( cp::Renderer* const       renderer,
                  const widgetProperties_t& properties,
                  const std::string&        command ) noexcept;
    void
    Draw() noexcept override;

    void
    Execute() noexcept;

  protected:
    void
    PrintLastOutput( const std::string& output,
                     const uint64_t     outputPosition ) noexcept;

  protected:
    std::unique_ptr< cp::SystemCommand >                         command;
    std::chrono::time_point< std::chrono::system_clock >         commandStart;
    mutable std::chrono::time_point< std::chrono::system_clock > commandStop;
    std::string                                                  commandText;
    uint64_t                              lastOutputSize{ 0 };
    uint64_t                              startSpaceOfOutput{ 0 };
    std::string                           lastOutputTruncated;
    bool                                  doesLastOutputGlow{ false };
    std::chrono::steady_clock::time_point lastOutputUpdate;

    uint64_t  glowTimeInMs{ 250 };
    modList_t commandOutput{ cp::Foreground_LightGray };
    modList_t commandOutputFinished{ cp::Foreground_DarkGray };
    modList_t commandOutputGlow{ cp::Foreground_Green, cp::Style_Bold,
                                 cp::Style_Underline };
    modList_t inactive{ cp::Foreground_LightGray };
    modList_t commandFinished{ cp::Foreground_DarkGray, cp::Style_Bold };
    modList_t commandFailed{ cp::Foreground_LightRed, cp::Style_Bold };
    modList_t commandValue{ cp::Foreground_Green };
    modList_t timeValueFailed{ cp::Foreground_Red, cp::Style_Bold };
    modList_t timeValueFinished{ cp::Foreground_Blue };
    modList_t timeValue{ cp::Foreground_LightYellow, cp::Style_Bold };
    modList_t timeValueBrackets{ cp::Foreground_DarkGray, cp::Style_Bold };
};
} // namespace cui
