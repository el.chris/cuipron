#pragma once

#include "cui/BaseWidget.hpp"
#include "cui/widgetProperties_t.hpp"
#include "cuipron/Renderer.hpp"
#include "cuipron/vec2.hpp"

#include <algorithm>
#include <condition_variable>
#include <mutex>

namespace cui
{
class Cui
{
  public:
    Cui( const uint16_t lineSpacing ) noexcept;
    Cui( const Cui& ) = delete;
    Cui( Cui&& rhs )  = delete;

    Cui&
    operator=( const Cui& ) = delete;
    Cui&
    operator=( Cui&& rhs ) = delete;

    ~Cui();

    cp::vec2
    GetStartupCursorPosition() const noexcept;

    template < typename T, typename... Targs >
    std::shared_ptr< T >
    Create( const cui::widgetProperties_t properties,
            Targs&&... args ) noexcept;

    void
    Remove( BaseWidget* const widget ) noexcept;

    void
    WaitForExitSignal() const noexcept;
    void
    SendExitSignal() const noexcept;

  private:
    void
    Draw() const noexcept;
    void
    AddLineSpacing( const uint16_t n ) const noexcept;

  private:
    uint8_t                    fps = 60u;
    cp::vec2                   startupCursorPosition;
    std::vector< BaseWidget* > widgets;
    cp::Renderer               renderer;

    mutable bool                    hasExitSignal{ false };
    mutable std::mutex              signalMutex;
    mutable std::condition_variable signalCondition;
};
} // namespace cui

#include "cui/Cui.inl"
