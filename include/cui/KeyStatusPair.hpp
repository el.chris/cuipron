#pragma once

#include "cui/BaseWidget.hpp"
#include "cuipron/ConsoleFont.hpp"

namespace cui
{
class KeyStatusPair : public BaseWidget
{
  public:
    KeyStatusPair( cp::Renderer* const       renderer,
                   const widgetProperties_t& properties ) noexcept;

    void
    Draw() noexcept override;
    void
    SetTitle( const set_t< std::string >& title ) noexcept;
    void
    SetValue( const set_t< std::string >& value ) noexcept;

  private:
    set_t< std::string > titleCallback;
    set_t< std::string > valueCallback;

    std::string                           oldValue;
    std::chrono::steady_clock::time_point lastUpdate;
    uint64_t                              glowTimeInMs{ 250 };

    modList_t titleStyle{ cp::Background_Default, cp::Foreground_Yellow };
    modList_t valueStyle{ cp::Background_Default, cp::Foreground_LightGray };
    modList_t valueUpdatedStyle{ cp::Background_Default,
                                 cp::Foreground_LightGreen,
                                 cp::Style_Underline };
};
} // namespace cui
