#pragma once

#include "cui/BaseWidget.hpp"
#include "cuipron/ConsoleFont.hpp"

namespace cui
{
class KeyValuePair : public BaseWidget
{
  public:
    KeyValuePair( cp::Renderer* const       renderer,
                  const widgetProperties_t& properties ) noexcept;

    void
    Draw() noexcept override;
    void
    SetTitle( const set_t< std::string >& title ) noexcept;
    void
    SetValue( const set_t< std::string >& value ) noexcept;
    void
    SetUnit( const set_t< std::string >& unit ) noexcept;

  private:
    set_t< std::string > titleCallback;
    set_t< std::string > valueCallback;
    set_t< std::string > unitCallback;

    modList_t titleStyle{ cp::Background_Default, cp::Foreground_Yellow };
    modList_t valueStyle{ cp::Background_Default, cp::Foreground_LightCyan };
    modList_t unitStyle{ cp::Background_Default, cp::Foreground_LightBlue };
};
} // namespace cui
