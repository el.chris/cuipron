namespace cp
{
template < typename T >
inline std::enable_if_t< !std::is_convertible_v< T, std::string > >
Renderer::SetContentValue( const size_t x, const size_t y,
                           const T& value ) noexcept
{
    SetContentValue( x, y, std::to_string( value ) );
}

template < typename T >
inline std::enable_if_t< std::is_convertible_v< T, std::string > >
Renderer::SetContentValue( const size_t x, const size_t y,
                           const T& value ) noexcept
{
    std::string v         = static_cast< std::string >( value );
    auto        lineWidth = this->currentContent->size();
    if ( lineWidth <= x ) return;
    if ( this->currentContent->at( x ).size() <= y ) return;

    for ( size_t posX = x, limit = std::min( x + v.size(), lineWidth );
          posX < limit; ++posX )
        this->currentContent->at( posX )[y].character = v[posX - x];
}
} // namespace cp
