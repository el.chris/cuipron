#pragma once

#include "cuipron/ConsoleControl.hpp"
#include "cuipron/ConsoleFont.hpp"

#include <atomic>
#include <chrono>
#include <functional>
#include <map>
#include <mutex>
#include <string>
#include <thread>
#include <type_traits>
#include <vector>


namespace cp
{
class Renderer
{
  public:
    struct element_t
    {
        char                       character{ '\0' };
        std::vector< ConsoleFont > modifier;
    };

    using content_t            = std::vector< std::vector< element_t > >;
    using contentModificator_t = std::function< void() >;

    Renderer( const contentModificator_t& contentModificator,
              const uint8_t               fps ) noexcept;
    ~Renderer();

    template < typename T >
    std::enable_if_t< !std::is_convertible_v< T, std::string > >
    SetContentValue( const size_t x, const size_t y, const T& value ) noexcept;

    template < typename T >
    std::enable_if_t< std::is_convertible_v< T, std::string > >
    SetContentValue( const size_t x, const size_t y, const T& value ) noexcept;

    void
    ClearRange( const size_t x, const size_t y, const size_t length ) noexcept;

    void
    AddContentModifier( const size_t x, const size_t y,
                        const std::vector< Modifier >& modifier ) noexcept;
    void
    ResetContentModifier( const size_t x, const size_t y,
                          const std::vector< Modifier >& modifier ) noexcept;
    vec2
    GetDimensions() const noexcept;

  private:
    void
    Draw() noexcept;
    void
    Refresh() noexcept;

  private:
    ConsoleControl            consoleControl;
    contentModificator_t      contentModificator;
    std::chrono::microseconds waitInFrame{ 0 };
    std::atomic_bool          keepRunning{ true };
    std::thread               renderThread;
    vec2                      currentTerminalSize;

    content_t  contentBuffer1;
    content_t  contentBuffer2;
    content_t* currentContent{ &contentBuffer1 };
    content_t* previousContent{ &contentBuffer2 };
};
} // namespace cp

#include "cuipron/Renderer.inl"
