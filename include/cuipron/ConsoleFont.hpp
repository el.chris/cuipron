#pragma once

#include <iostream>
#include <type_traits>

namespace cp
{
enum Modifier : int
{
    Style_Reset     = 0,
    Style_Normal    = 22,
    Style_Bold      = 1,
    Style_Faint     = 2,
    Style_Italic    = 3,
    Style_Underline = 4,
    Style_Blink     = 5,
    Style_Inverted  = 6,
    Style_Hidden    = 8,

    Foreground_Default      = 39,
    Foreground_Black        = 30,
    Foreground_Red          = 31,
    Foreground_Green        = 32,
    Foreground_Yellow       = 33,
    Foreground_Blue         = 34,
    Foreground_Magenta      = 35,
    Foreground_Cyan         = 36,
    Foreground_LightGray    = 37,
    Foreground_DarkGray     = 90,
    Foreground_LightRed     = 91,
    Foreground_LightGreen   = 92,
    Foreground_LightYellow  = 93,
    Foreground_LightBlue    = 94,
    Foreground_LightMagenta = 95,
    Foreground_LightCyan    = 96,
    Foreground_White        = 97,

    Background_Default      = 49,
    Background_Black        = 40,
    Background_Red          = 41,
    Background_Green        = 42,
    Background_Yellow       = 43,
    Background_Blue         = 44,
    Background_Magenta      = 45,
    Background_Cyan         = 46,
    Background_LightGray    = 47,
    Background_DarkGray     = 100,
    Background_LightRed     = 101,
    Background_LightGreen   = 102,
    Background_LightYellow  = 103,
    Background_LightBlue    = 104,
    Background_LightMagenta = 105,
    Background_LightCyan    = 106,
    Background_White        = 107,
};

class ConsoleFont
{
  public:
    ConsoleFont() = default;
    ConsoleFont( const Modifier modifier ) noexcept;

    friend std::ostream&
    operator<<( std::ostream&, const ConsoleFont& ) noexcept;

  private:
    int modifier = -1;
};

std::ostream&
operator<<( std::ostream& out, const ConsoleFont& property ) noexcept;

} // namespace cp
