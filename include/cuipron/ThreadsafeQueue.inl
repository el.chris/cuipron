namespace cp
{

template < typename T >
inline void
ThreadsafeQueue< T >::push( const T& value ) noexcept
{
    std::lock_guard< std::mutex > lock( this->mutex );
    this->queue.push( value );
}

template < typename T >
inline std::optional< T >
ThreadsafeQueue< T >::pop() noexcept
{
    std::lock_guard< std::mutex > lock( this->mutex );
    if ( this->queue.empty() ) return std::nullopt;

    auto retVal = this->queue.front();
    this->queue.pop();
    return retVal;
}

template < typename T >
inline bool
ThreadsafeQueue< T >::empty() const noexcept
{
    std::lock_guard< std::mutex > lock( this->mutex );
    return this->queue.empty();
}
} // namespace cp

