cmake_minimum_required( VERSION 3.6 )

project(cuipron)

if ( NOT CMAKE_BUILD_TYPE )
    set(CMAKE_BUILD_TYPE "Debug")
endif()

find_package(Threads)

option(STATIC_LINKAGE "" OFF)
option(USE_SANITIZER "use address and undefined behavior sanitizer" OFF)
option(USE_TCMALLOC  "use tcmalloc from the google performance tools" OFF)

file ( GLOB_RECURSE CUIPRON_SRC 
    "${CMAKE_CURRENT_SOURCE_DIR}/src/cuipron/*.cpp"
)
file ( GLOB_RECURSE CUI_SRC 
    "${CMAKE_CURRENT_SOURCE_DIR}/src/cui/*.cpp"
)

if(STATIC_LINKAGE)
    add_library(cuipron STATIC ${CUIPRON_SRC})
else()
    add_library(cuipron SHARED ${CUIPRON_SRC})
endif()

target_include_directories(cuipron 
    PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}/include/"
)

if(STATIC_LINKAGE)
    add_library(cui STATIC ${CUI_SRC})
else()
    add_library(cui SHARED ${CUI_SRC})
endif()
target_include_directories(cui
    PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}/include/"
)
target_link_libraries(cui cuipron)

add_executable(example "${CMAKE_CURRENT_SOURCE_DIR}/src/example.cpp")
target_link_libraries(example cui Threads::Threads)

if(CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /std:c++latest")
else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++2a")
    if ( CMAKE_BUILD_TYPE MATCHES "Debug" )
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -ggdb -O0 -W -Wall -pedantic -Wextra -Wconversion")
        if ( USE_SANITIZER )
            set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address")
            set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -lasan")
        endif()
    else()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native -O3")
        if ( USE_TCMALLOC )
            set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -ltcmalloc")
        endif()
    endif()
endif()

message("")
message("")
message("       Build Properties")
message("")
message("           type........................: " ${CMAKE_BUILD_TYPE})
message("           project name................: " ${CMAKE_PROJECT_NAME})
message("           number of cpus..............: " ${PROCESSOR_COUNT})
message("           c compiler..................: " ${CMAKE_C_COMPILER})
message("           c++ compiler................: " ${CMAKE_CXX_COMPILER})
message("           c++ flags...................: " ${CMAKE_CXX_FLAGS})
message("           linker flags................: " ${CMAKE_EXE_LINKER_FLAGS})
message("           clang-tidy..................: " ${CLANG_TIDY_EXE})
message("")
message("       Options")
message("           STATIC_LINKAGE..............: " ${STATIC_LINKAGE})
message("           USE_SANITIZER...............: " ${USE_SANITIZER})
message("           USE_TCMALLOC................: " ${USE_TCMALLOC})
message("")
if (CMAKE_BUILD_TYPE MATCHES "Debug")
message("       hint: if you want to build in the release mode simply add -DCMAKE_BUILD_TYPE=Release")
endif (CMAKE_BUILD_TYPE MATCHES "Debug")
message("")

