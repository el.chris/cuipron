#include "cuipron/Renderer.hpp"

#include "cuipron/CatchIntSignal.hpp"
#include "cuipron/ConsoleFont.hpp"

#include <algorithm>
#include <iostream>
#include <thread>

namespace cp
{
Renderer::Renderer( const contentModificator_t& contentModificator,
                    const uint8_t               fps ) noexcept
    : contentModificator( contentModificator ), waitInFrame( 1000000 / fps ),
      currentTerminalSize( consoleControl.GetTerminalDimensions() )
{
    this->renderThread = std::thread( [this] {
        this->consoleControl.HideCursor();
        this->Refresh();
        while ( this->keepRunning.load() )
        {
            this->Draw();
            std::this_thread::sleep_for( this->waitInFrame );
        }
    } );
}

Renderer::~Renderer()
{
    this->keepRunning.store( false );
    this->renderThread.join();
    this->consoleControl.ShowCursor();
}

void
Renderer::ClearRange( const size_t x, const size_t y,
                      const size_t length ) noexcept
{
    auto lineWidth = this->currentContent->size();
    if ( !( lineWidth <= x ) && !( this->currentContent->at( x ).size() <= y ) )
        this->currentContent->at( x )[y].modifier.emplace_back( Style_Reset );

    this->SetContentValue( x, y, std::string( length, ' ' ) );
}

void
Renderer::Draw() noexcept
{
    this->contentModificator();

    this->consoleControl.SetCursorPosition( vec2{ 1, 1 } );
    auto dimension = this->consoleControl.GetTerminalDimensions();
    for ( uint64_t y = 0; y < dimension.y; ++y )
        for ( uint64_t x = 0; x < dimension.x; ++x )
        {
            auto v = this->currentContent->at( x )[y];
            if ( v.modifier.empty() && v.character == '\0' ) continue;
            this->consoleControl.SetCursorPosition(
                vec2{ static_cast< uint16_t >( x + 1u ),
                      static_cast< uint16_t >( y + 1u ) } );
            for ( auto& m : v.modifier )
                std::cout << m;
            std::cout << v.character;
        }

    this->Refresh();
}

void
Renderer::Refresh() noexcept
{
    this->previousContent = this->currentContent;
    this->currentContent  = ( this->currentContent == &this->contentBuffer1 )
                               ? &this->contentBuffer2
                               : &this->contentBuffer1;
    this->consoleControl.Refresh();

    vec2 newTerminalSize = this->consoleControl.GetTerminalDimensions();
    if ( this->currentTerminalSize != newTerminalSize )
    {
        this->currentTerminalSize = newTerminalSize;
        this->consoleControl.ClearTerminal();
    }

    this->currentContent->resize(
        this->consoleControl.GetTerminalDimensions().x );

    for ( auto& e : *this->currentContent )
    {
        e.resize( this->consoleControl.GetTerminalDimensions().y );
        std::fill( e.begin(), e.end(), element_t{ '\0', {} } );
    }
}

void
Renderer::AddContentModifier( const size_t x, const size_t y,
                              const std::vector< Modifier >& modifier ) noexcept
{
    auto lineWidth = this->currentContent->size();
    if ( lineWidth <= x ) return;
    if ( this->currentContent->at( x ).size() <= y ) return;

    for ( auto& m : modifier )
        this->currentContent->at( x )[y].modifier.emplace_back( m );
}

void
Renderer::ResetContentModifier(
    const size_t x, const size_t y,
    const std::vector< Modifier >& modifier ) noexcept
{
    auto lineWidth = this->currentContent->size();
    if ( lineWidth <= x ) return;
    if ( this->currentContent->at( x ).size() <= y ) return;

    this->currentContent->at( x )[y].modifier.emplace_back( Style_Reset );
    for ( auto& m : modifier )
        this->currentContent->at( x )[y].modifier.emplace_back( m );
}

vec2
Renderer::GetDimensions() const noexcept
{
    return this->consoleControl.GetTerminalDimensions();
}


} // namespace cp
