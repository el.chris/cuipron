#include "cuipron/ConsoleControl.hpp"

#ifndef _WIN32
#include <sys/signal.h>
#include <unistd.h>
#else
#include <signal.h>
#include <stdlib.h>
#endif


void
signalHandler( int sig )
{
    switch ( sig )
    {
        case SIGINT:
            cp::ConsoleControl().ShowCursor();
            break;
        default:
            cp::ConsoleControl().ShowCursor();
            break;
    }

    _exit( 0 );
}
