#include "cui/CommandState.hpp"
#include "cui/Cui.hpp"
#include "cui/KeyStatusPair.hpp"
#include "cui/KeyValuePair.hpp"
#include "cui/Label.hpp"
#include "cui/Line.hpp"
#include "cui/ProgressBar.hpp"
#include "cui/StaticSymbol.hpp"
#include "cui/widgetProperties_t.hpp"
#include "cuipron/ConsoleFont.hpp"
#include "cuipron/Renderer.hpp"
#include "cuipron/SystemCommand.hpp"

#include <iostream>
#include <thread>

int
main()
{
    uint16_t lineSpacing{ 20u };
    cui::Cui cui( lineSpacing );
    auto     pos = cui.GetStartupCursorPosition();

    auto progressBar = cui.Create< cui::ProgressBar >(
        { .size     = { 40, 1 },
          .position = { pos.x,
                        static_cast< uint16_t >( pos.y - lineSpacing ) } } );

    progressBar->SetDescription( [&] {
        static int i = 0;
        i++;
        return std::to_string( i );
    } );
    progressBar->SetProgress( [] {
        static int i = 0;
        return i++;
    } );

    auto commandState = cui.Create< cui::CommandState >(
        { .size     = { 160, 1 },
          .position = { pos.x,
                        static_cast< uint16_t >( pos.y - lineSpacing + 1 ) } },
        "pakku -Syu" );

    auto sleepState = cui.Create< cui::CommandState >(
        { .size     = { 160, 1 },
          .position = { pos.x,
                        static_cast< uint16_t >( pos.y - lineSpacing + 2 ) } },
        "sleep 2" );
    sleepState->Execute();
    auto sleepStateFail = cui.Create< cui::CommandState >(
        { .size     = { 160, 1 },
          .position = { pos.x,
                        static_cast< uint16_t >( pos.y - lineSpacing + 3 ) } },
        "sleepFail 2" );
    sleepStateFail->Execute();


    auto label = cui.Create< cui::Label >(
        { .size     = { 160, 1 },
          .position = {
              pos.x, static_cast< uint16_t >( pos.y - lineSpacing + 4 ) } } );
    label->SetText( [] {
        static int i = 0;
        ++i;
        if ( ( i / 100 ) % 3 == 0 )
            return "Hello World ";
        else if ( ( i / 100 ) % 3 == 1 )
            return "123 ";
        else
            return "fublaballaalassdfsdf";
    } );

    auto value = cui.Create< cui::KeyValuePair >(
        { .size     = { 160, 1 },
          .position = {
              pos.x, static_cast< uint16_t >( pos.y - lineSpacing + 5 ) } } );
    value->SetTitle( [] { return "speed"; } );
    value->SetValue( [] {
        static int i = 1;
        ++i;
        return std::to_string( i );
    } );
    value->SetUnit( [] { return "km/h"; } );

    auto status = cui.Create< cui::KeyStatusPair >(
        { .size     = { 160, 1 },
          .position = {
              pos.x, static_cast< uint16_t >( pos.y - lineSpacing + 6 ) } } );
    status->SetTitle( [] { return "state"; } );
    status->SetValue( [] {
        static int i = 0;
        ++i;
        if ( ( i / 100 ) % 3 == 0 )
            return "Hello World ";
        else if ( ( i / 100 ) % 3 == 1 )
            return "123 ";
        else
            return "fublaballaalassdfsdf";
    } );

    auto line = cui.Create< cui::Line >(
        { .size     = { 160, 1 },
          .position = {
              pos.x, static_cast< uint16_t >( pos.y - lineSpacing + 7 ) } } );
    line->SetUp( cui::Line::Horizontal, cui::Line::Fat, 110 );

    auto symbol = cui.Create< cui::StaticSymbol >(
        { .size     = { 160, 1 },
          .position = {
              static_cast< uint16_t >( pos.x + 5u ),
              static_cast< uint16_t >( pos.y - lineSpacing + 9 ) } } );
    symbol->SetContent( "O" );


    std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
    commandState->Execute();

    cui.WaitForExitSignal();

    return 0;
}
