#include "cui/CommandState.hpp"

#include "cuipron/SystemCommand.hpp"

#include <chrono>
#include <iomanip>
#include <sstream>

namespace cui
{
CommandState::CommandState( cp::Renderer* const       renderer,
                            const widgetProperties_t& properties,
                            const std::string&        command ) noexcept
    : BaseWidget( renderer, properties ), commandText( command )
{
    this->commandStart = std::chrono::system_clock::now();
    this->commandStop  = this->commandStart;
}

void
CommandState::Draw() noexcept
{
    BaseWidget::Draw();

    if ( this->command && !this->command->IsAsyncExecuteFinished() )
        this->commandStop = std::chrono::system_clock::now();

    double ms = static_cast< double >(
                    std::chrono::duration_cast< std::chrono::milliseconds >(
                        this->commandStop - this->commandStart )
                        .count() ) /
                1000.0;

    std::stringstream ss;
    ss << std::fixed << std::setprecision( 2 ) << ms << "s";

    modList_t timeStyle = this->inactive;
    if ( this->command )
    {

        if ( this->command->IsAsyncExecuteFinished() &&
             this->command->IsAsyncExecuteSuccessful() )
            timeStyle = this->timeValueFinished;
        else if ( this->command->IsAsyncExecuteFinished() &&
                  !this->command->IsAsyncExecuteSuccessful() )
            timeStyle = this->timeValueFailed;
        else
            timeStyle = this->timeValue;
    }

    if ( this->command )
    {
        this->renderer->ResetContentModifier(
            this->position.x, this->position.y, this->timeValueBrackets );
        this->renderer->ResetContentModifier( this->position.x + 3,
                                              this->position.y, timeStyle );
        this->renderer->ResetContentModifier(
            this->position.x + 3 + ss.str().size(), this->position.y,
            this->timeValueBrackets );

        auto style = this->commandValue;
        if ( this->command->IsAsyncExecuteFinished() )
            style = ( this->command->GetExitCode() == 0 )
                        ? this->commandFinished
                        : this->commandFailed;

        this->renderer->ResetContentModifier(
            this->position.x + 6 + ss.str().size(), this->position.y, style );
    }
    else
    {
        this->renderer->ResetContentModifier(
            this->position.x, this->position.y, this->inactive );
    }

    std::string commandInfo =
        " [ " + ss.str() + " ] " + this->commandText + " ";
    auto commandOutputPos = commandInfo.size();

    commandInfo.resize( this->size.x, ' ' );
    this->renderer->SetContentValue( this->position.x, this->position.y,
                                     commandInfo );

    if ( this->command &&
         commandOutputPos < static_cast< uint64_t >( this->size.x ) )
    {
        auto output = this->command->GetOutput();
        if ( !output.empty() )
            this->PrintLastOutput( output.back(), commandOutputPos );
    }
}

void
CommandState::PrintLastOutput( const std::string& output,
                               const uint64_t     outputPosition ) noexcept
{
    if ( this->lastOutputSize != this->command->GetOutput().size() )
    {
        this->lastOutputSize      = this->command->GetOutput().size();
        this->lastOutputTruncated = output;
        auto pos = this->lastOutputTruncated.find_first_not_of( " " );
        if ( pos != std::string::npos )
            this->lastOutputTruncated = this->lastOutputTruncated.substr( pos );
        this->lastOutputTruncated.resize( this->size.x - outputPosition, ' ' );
        this->doesLastOutputGlow = true;
        this->lastOutputUpdate   = std::chrono::steady_clock::now();
        auto startSpace = this->lastOutputTruncated.find_last_not_of( ' ' );
        this->startSpaceOfOutput = ( startSpace != std::string::npos )
                                       ? startSpace + 1
                                       : this->lastOutputTruncated.size();
    }

    uint64_t passedTime = static_cast< uint64_t >(
        std::chrono::duration_cast< std::chrono::milliseconds >(
            std::chrono::steady_clock::now() - this->lastOutputUpdate )
            .count() );

    auto style =
        ( doesLastOutputGlow ) ? this->commandOutputGlow : this->commandOutput;
    if ( this->command->IsAsyncExecuteFinished() &&
         this->command->GetExitCode() == 0 )
        style = this->commandOutputFinished;

    this->renderer->ResetContentModifier( this->position.x + outputPosition,
                                          this->position.y, style );
    this->renderer->ResetContentModifier(
        this->position.x + outputPosition + this->startSpaceOfOutput,
        this->position.y, this->commandOutput );
    this->renderer->SetContentValue( this->position.x + outputPosition,
                                     this->position.y,
                                     this->lastOutputTruncated );

    if ( passedTime >= this->glowTimeInMs ) this->doesLastOutputGlow = false;
}

void
CommandState::Execute() noexcept
{
    this->commandStart = std::chrono::system_clock::now();
    this->command =
        std::make_unique< cp::SystemCommand >( this->commandText + " 2>&1 " );
    this->command->AsyncExecute();
}


} // namespace cui
