#include "cui/KeyStatusPair.hpp"

namespace cui
{
KeyStatusPair::KeyStatusPair( cp::Renderer* const       renderer,
                              const widgetProperties_t& properties ) noexcept
    : BaseWidget( renderer, properties )
{
}

void
KeyStatusPair::SetTitle( const set_t< std::string >& title ) noexcept
{
    this->titleCallback = title;
}

void
KeyStatusPair::SetValue( const set_t< std::string >& value ) noexcept
{
    this->valueCallback = value;
}

void
KeyStatusPair::Draw() noexcept
{
    BaseWidget::Draw();

    std::string title, value;
    if ( this->titleCallback ) title = this->titleCallback();
    if ( this->valueCallback ) value = this->valueCallback();

    auto titleSize = title.size();

    if ( this->oldValue != value )
    {
        auto oldValueSize = this->oldValue.size();
        auto valueSize    = value.size();
        if ( oldValueSize > valueSize )
            this->renderer->ClearRange( position.x + titleSize + valueSize + 1,
                                        position.y, oldValueSize - valueSize );

        this->oldValue   = value;
        this->lastUpdate = std::chrono::steady_clock::now();
    }

    uint64_t passedTime = static_cast< uint64_t >(
        std::chrono::duration_cast< std::chrono::milliseconds >(
            std::chrono::steady_clock::now() - this->lastUpdate )
            .count() );

    this->renderer->ResetContentModifier( this->position.x, this->position.y,
                                          this->titleStyle );
    this->renderer->SetContentValue( this->position.x, this->position.y,
                                     title );

    if ( passedTime < this->glowTimeInMs )
    {
        this->renderer->ResetContentModifier( this->position.x + titleSize + 1,
                                              this->position.y,
                                              this->valueUpdatedStyle );
    }
    else
    {
        this->renderer->ResetContentModifier( this->position.x + titleSize + 1,
                                              this->position.y,
                                              this->valueStyle );
    }

    this->renderer->SetContentValue( this->position.x + titleSize + 1,
                                     this->position.y, value );
}

} // namespace cui
