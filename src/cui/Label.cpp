#include "cui/Label.hpp"

#include "cui/BaseWidget.hpp"

#include <chrono>

namespace cui
{
Label::Label( cp::Renderer* const       renderer,
              const widgetProperties_t& properties ) noexcept
    : BaseWidget( renderer, properties )
{
}

void
Label::Draw() noexcept
{
    BaseWidget::Draw();

    if ( this->textCallback )
    {
        std::string newText = this->textCallback();
        if ( newText != this->currentText )
        {
            this->cleanupPosition =
                std::max( this->cleanupPosition, this->currentTextSize );
            this->currentPosition          = 0;
            this->underlineRetreatPosition = 0;
            this->currentText              = newText;
            this->currentTextSize          = newText.size();
            this->lastUpdate               = std::chrono::steady_clock::now();
        }
    }

    uint64_t passedTime = static_cast< uint64_t >(
        std::chrono::duration_cast< std::chrono::milliseconds >(
            std::chrono::steady_clock::now() - this->lastUpdate )
            .count() );

    if ( ( this->currentPosition != this->currentTextSize &&
           this->updateSpeedInMs < passedTime ) ||
         ( this->underlineRetreatPosition != 0 &&
           this->underlineRetreatPosition < this->currentPosition ) )
    {
        this->currentPosition =
            std::min( this->currentTextSize, this->currentPosition + 1 );
        if ( this->currentPosition >= this->currentTextSize &&
             this->currentPosition > this->underlineRetreatPosition )
            this->underlineRetreatPosition++;
        this->lastUpdate = std::chrono::steady_clock::now();
    }

    if ( this->cleanupPosition > 0 &&
         this->currentTextSize < this->cleanupPosition )
    {
        this->renderer->ResetContentModifier(
            this->position.x + this->cleanupPosition - 1, this->position.y,
            this->labelOld );
        this->renderer->SetContentValue( this->position.x +
                                             this->cleanupPosition - 1,
                                         this->position.y, " " );
        --this->cleanupPosition;
    }
    else if ( this->currentTextSize == this->cleanupPosition )
    {
        this->cleanupPosition = 0;
    }

    if ( this->currentPosition != this->currentTextSize )
    {
        if ( this->currentPosition != 0 )
        {
            this->renderer->ResetContentModifier(
                this->position.x, this->position.y, this->labelBuildup );
            this->renderer->SetContentValue(
                this->position.x, this->position.y,
                this->currentText.substr( 0, this->currentPosition ) );
        }

        if ( this->currentPosition + 1 < this->currentTextSize )
        {
            this->renderer->ResetContentModifier(
                this->position.x + this->currentPosition, this->position.y,
                this->labelBuildupGlow );
            this->renderer->SetContentValue(
                this->position.x + this->currentPosition, this->position.y,
                this->currentText.substr( this->currentPosition, 1 ) );
        }
    }
    else
    {
        this->renderer->ResetContentModifier(
            this->position.x, this->position.y, this->labelBuildup );
        this->renderer->ResetContentModifier(
            this->position.x + this->currentPosition -
                this->underlineRetreatPosition,
            this->position.y, this->labelNormal );
        this->renderer->SetContentValue( this->position.x, this->position.y,
                                         this->currentText );
    }
}

void
Label::SetText( const set_t< std::string >& text ) noexcept
{
    this->textCallback = text;
}
} // namespace cui
