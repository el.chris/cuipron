#include "cui/StaticSymbol.hpp"

#include "cui/BaseWidget.hpp"

namespace cui
{
StaticSymbol::StaticSymbol( cp::Renderer* const       renderer,
                            const widgetProperties_t& properties ) noexcept
    : BaseWidget( renderer, properties )
{
}

void
StaticSymbol::SetContent( const std::string& value ) noexcept
{
    this->content = value;
}

void
StaticSymbol::Draw() noexcept
{
    BaseWidget::Draw();
    this->renderer->ResetContentModifier( this->position.x, this->position.y,
                                          this->style );
    this->renderer->SetContentValue( this->position.x, this->position.y,
                                     this->content );
}


} // namespace cui
