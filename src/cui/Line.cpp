#include "cui/Line.hpp"

#include "cui/BaseWidget.hpp"

namespace cui
{
Line::Line( cp::Renderer* const       renderer,
            const widgetProperties_t& properties ) noexcept
    : BaseWidget( renderer, properties )
{
}

void
Line::SetUp( const Type type, const Style style,
             const uint64_t length ) noexcept
{
    this->lineType   = type;
    this->lineLength = length;
    this->lineStyle  = style;
}

void
Line::Draw() noexcept
{
    BaseWidget::Draw();

    if ( this->lineType == Type::Horizontal )
    {
        auto dimension = cp::ConsoleControl().GetTerminalDimensions();
        if ( this->position.x > dimension.x ) return;

        this->renderer->ResetContentModifier(
            this->position.x, this->position.y,
            ( this->lineStyle == Style::Fat ) ? this->fatLineStyle
                                              : this->thinLineStyle );

        uint64_t adjustedLength = ( this->position.x + this->lineLength >
                                    static_cast< uint64_t >( dimension.x ) )
                                      ? dimension.x - this->position.x
                                      : this->lineLength;

        std::string line( adjustedLength,
                          ( this->lineStyle == Style::Fat ) ? ' ' : '-' );
        this->renderer->SetContentValue( this->position.x, this->position.y,
                                         line );
    }
    else if ( this->lineType == Type::Vertical )
    {
        auto dimension = cp::ConsoleControl().GetTerminalDimensions();
        if ( this->position.y > dimension.y ) return;

        uint64_t adjustedLength = ( this->position.y + this->lineLength >
                                    static_cast< uint64_t >( dimension.y ) )
                                      ? dimension.y - this->position.y
                                      : this->lineLength;

        for ( uint64_t i = 0; i < adjustedLength; ++i )
        {
            this->renderer->ResetContentModifier(
                this->position.x, this->position.y + i,
                ( this->lineStyle == Style::Fat ) ? this->fatLineStyle
                                                  : this->thinLineStyle );

            this->renderer->SetContentValue(
                this->position.x, this->position.y + i,
                std::string( 1,
                             ( this->lineStyle == Style::Fat ) ? ' ' : '|' ) );
        }
    }
}
} // namespace cui
