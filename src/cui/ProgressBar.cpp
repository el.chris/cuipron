#include "cui/ProgressBar.hpp"

#include "cui/BaseWidget.hpp"
#include "cuipron/ConsoleFont.hpp"
#include "cuipron/Renderer.hpp"

namespace cui
{
ProgressBar::ProgressBar( cp::Renderer* const       renderer,
                          const widgetProperties_t& properties ) noexcept
    : BaseWidget( renderer, properties )
{
}

void
ProgressBar::SetProgress( const set_t< int >& progress ) noexcept
{
    this->setter.push(
        [this, progress] { this->progressCallback = progress; } );
}

void
ProgressBar::SetDescription( const set_t< std::string >& description ) noexcept
{
    this->setter.push(
        [this, description] { this->descriptionCallback = description; } );
}

void
ProgressBar::Draw() noexcept
{
    BaseWidget::Draw();

    auto progress = ( this->progressCallback ) ? this->progressCallback() : 0u;
    auto description = ( this->descriptionCallback )
                           ? this->descriptionCallback()
                           : std::string();

    for ( auto y = this->position.y; y < this->position.y + this->size.y; ++y )
    {
        this->renderer->ResetContentModifier( this->position.x, y,
                                              ( progress >= 100 )
                                                  ? this->progressBarFinished
                                                  : this->progressBarActive );

        this->renderer->ResetContentModifier( this->position.x +
                                                  this->size.x * progress / 100,
                                              y, this->progressBarInactive );

        this->renderer->AddContentModifier( this->position.x + this->size.x, y,
                                            { cp::Style_Reset } );

        this->renderer->SetContentValue( this->position.x, y,
                                         std::string( this->size.x, ' ' ) );
    }

    this->renderer->SetContentValue(
        this->position.x + ( this->size.x - description.size() ) / 2,
        this->position.y + this->size.y / 2, description );
}

} // namespace cui
